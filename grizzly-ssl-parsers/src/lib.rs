//! Convenience methods for parsing cryptographic files at runtime, for [`GrizzlySSL`].
//!
//! This crate adds methods, intended to be run by a build script, to read, then parse the provided files,
//! and create a [`String`] that can be written to a generated file. This generated code will define a
//! `static` variable that contains all the information necessary for [`GrizzlySSL`] to perform the
//! corresponding cryptographic operations, for the generated type.
//!
//! ## Example
//!
//! Build script (`build.rs`):
//! ```ignore
//! use std::fs::File;
//! use std::io::Write;
//! use std::path::PathBuf;
//! use grizzly_ssl_parsers::parse_trust_anchors;
//!
//! fn main() {
//!     let in_path = PathBuf::from(std::env::var("CARGO_MANIFEST_DIR").unwrap())
//!         .join("certs/trust_anchor.pem");
//!     let out_path = PathBuf::from(std::env::var("OUT_DIR").unwrap())
//!         .join("generated.rs");
//!     println!("cargo:rerun-if-changed={}", in_path.to_str().unwrap());
//!
//!     let code = parse_trust_anchors("TRUST_ANCHORS", vec![in_path]);
//!     let mut file = File::create(out_path).unwrap();
//!     writeln!(file, "{}", code).unwrap();
//! }
//! ```
//!
//! Generated code:
//! ```
//! /*
//! - the variable is always defined as public
//! |      - the variable is static, so that it is baked into the executable
//! |      |   - variable name is that of the `var_name` parameter
//! |      |   |              - a slice of trust anchors
//! |      |   |              |                                               */
//! pub static TRUST_ANCHORS: &[grizzly_ssl::x509::TrustAnchor] = &[
//!     // The trust anchor will have the data from the provided certificate entered in.
//!     grizzly_ssl::x509::TrustAnchor::new(
//!         &[/* dn */],
//!         true,
//!         grizzly_ssl::x509::PublicKey::Rsa {
//!             n: &[/* modulus */],
//!             e: &[/* exponent */]
//!         }
//!     )
//! ];
//! ```
//!
//! [`GrizzlySSL`]: ../grizzly_ssl/index.html
//! [`String`]: https://doc.rust-lang.org/nightly/std/string/struct.String.html

/// Parse a client certificate from a list of `certs`, and a `keyfile`, and generate corresponding Rust code.
///
/// This method will generate a single `ClientCertificate` object, with the variable name being that of the
/// `var_name` parameter.
pub fn parse_client_certificate(
	var_name: &str,
	certs: Vec<impl AsRef<std::path::Path>>,
	key_filepath: impl AsRef<std::path::Path>,
) -> String {
	use std::fs::File;
	use std::io::{BufReader, Read};

	let mut cert_objects = Vec::new();
	for cert in certs {
		let mut f = File::open(cert).expect("Could not read certificate file");
		let mut v = Vec::new();
		f.read_to_end(&mut v)
			.expect("Could not read certificate file");
		let (_, pem) = x509_parser::pem::pem_to_der(&v).expect("Could not parse certificate file");
		cert_objects.push(format!(
			"grizzly_ssl::x509::X509Certificate::new(&{:?})",
			pem.contents
		));
	}

	let file = File::open(key_filepath).expect("Could not read key file");
	let (pem, _) =
		x509_parser::pem::Pem::read(BufReader::new(file)).expect("Could not parse key file");

	match pem.label.as_str() {
		"RSA" => {
			let (_, (n_bitlen, p, q, dp, dq, iq)) =
				der_parser::ber::parse_ber_sequence_defined_g(|x| {
					let (x, _version) = der_parser::ber::parse_ber_integer(x)?;
					let (x, rsa_n) = der_parser::ber::parse_ber_integer(x)?;
					let (x, _rsa_e) = der_parser::ber::parse_ber_integer(x)?;
					let (x, _rsa_n) = der_parser::ber::parse_ber_integer(x)?;
					let (x, rsa_p) = der_parser::ber::parse_ber_integer(x)?;
					let (x, rsa_q) = der_parser::ber::parse_ber_integer(x)?;
					let (x, rsa_dp) = der_parser::ber::parse_ber_integer(x)?;
					let (x, rsa_dq) = der_parser::ber::parse_ber_integer(x)?;
					let (x, rsa_iq) = der_parser::ber::parse_ber_integer(x)?;

					let n_bitlen = rsa_n.as_biguint().unwrap().to_bytes_be().len() * 8;
					Ok((x, (n_bitlen, rsa_p, rsa_q, rsa_dp, rsa_dq, rsa_iq)))
				})(&pem.contents)
				.expect("Could not parse public key info");

			format!(
				r#"pub static {}: grizzly_ssl::x509::ClientCertificate = grizzly_ssl::x509::ClientCertificate::new(
						&[{}], 
						grizzly_ssl::x509::PrivateKey::Rsa {{
							n_bitlen: {:?},
							p: &{:?},
							q: &{:?},
							dp: &{:?},
							dq: &{:?},
							iq: &{:?},
						}}
					);"#,
				var_name,
				cert_objects.join(","),
				n_bitlen,
				p.as_biguint().unwrap().to_bytes_be(),
				q.as_biguint().unwrap().to_bytes_be(),
				dp.as_biguint().unwrap().to_bytes_be(),
				dq.as_biguint().unwrap().to_bytes_be(),
				iq.as_biguint().unwrap().to_bytes_be(),
			)
		}
		label => panic!("Unsupported PEM type: {}", label),
	}
}

/// Parse a list of trust anchors, from a list of `pem_files`, and generate corresponding Rust code.
///
/// This method will generate a slice of `TrustAnchor`s, with the variable name being that of the
/// `var_name` parameter.
pub fn parse_trust_anchors(var_name: &str, pem_files: Vec<impl AsRef<std::path::Path>>) -> String {
	use std::fs::File;
	use std::io::BufReader;

	let mut v = Vec::new();
	for filepath in pem_files {
		let file = File::open(filepath).expect("Could not read file");
		let (pem, _) =
			x509_parser::pem::Pem::read(BufReader::new(file)).expect("Could not parse PEM file");
		let cert = pem.parse_x509().expect("Could not parse PEM file");

		let dn = cert.subject();

		let oid = cert
			.tbs_certificate
			.subject_pki
			.algorithm
			.algorithm
			.to_id_string();
		match oid.as_str() {
			// RSA
			"1.2.840.113549.1.1.1" => {
				let (_, (modulus, exponent)) =
					der_parser::ber::parse_ber_sequence_defined_g(|x| {
						let (x, modulus) = der_parser::ber::parse_ber_integer(x)?;
						let (x, public_exponent) = der_parser::ber::parse_ber_integer(x)?;
						Ok((x, (modulus, public_exponent)))
					})(cert.tbs_certificate.subject_pki.subject_public_key.data)
					.expect("Could not parse public key info");

				v.push(format!(
						"grizzly_ssl::x509::TrustAnchor::new(&{:?}, {:?}, grizzly_ssl::x509::PublicKey::Rsa {{ n: &{:?}, e: &{:?} }})",
						dn.as_raw(),
						cert.tbs_certificate.is_ca(),
						modulus.as_biguint().unwrap().to_bytes_be(),
						exponent.as_biguint().unwrap().to_bytes_be(),
					))
			}
			oid => panic!("Unsupported signature algorithm: {}", oid),
		}
	}

	format!(
		"pub static {}: &[grizzly_ssl::x509::TrustAnchor] = &[{}];",
		var_name,
		v.join(",")
	)
}

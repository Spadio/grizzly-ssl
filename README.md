# GrizzlySSL

GrizzlySSL is a idiomatic Rust wrapper for [BearSSL](https://bearssl.org/index.html).

BearSSL is a small, highly portable, feature-rich SSL/TLS library that can be run on anything from a Windows or Linux device, to a small embedded system. With a minimal implementation coming in at ~20Kb flash size, and ~25Kb RAM usage, it can be run just about anywhere. Do note, however, that these figures are correct for the C source code, and may not be entirely accurate for GrizzlySSL (although there is no reason they should be very far off). 

As the main focus of GrizzlySSL is on embedded systems, the crate uses a `no_std` environment by default, although standard library features can be enabled using the `std` feature flag. See [Feature Flags](#feature-flags) for more information.

## Building

GrizzlySSL depends on the `bear-ssl-sys` crate to generate bindings, and statically link to, the BearSSL library. The build experience is set up to be as seamless as possible. The library should compile for any target (native or cross-compilation) through cargo, using the `--target` flag, or by specifying a target in the `.cargo/config` file.

## I/O

GrizzlySSL uses the [`bare-io`] library for handling I/O, which acts like the Rust standard library [`std::io`]
module (and with the `std` feature flag, is essentially just an alias to it), but changes the [`std::io::Error`]
type so that it can be used in `no_std` environments.

This means that the commonly used interface from the standard library can be used in GrizzlySSL, and with the `std` feature flag,
the types that implement [`bare_io::Read`] and [`bare_io::Write`] implement the [`std::io`] version of those traits (instead of the [`bare-io`] versions),
so the Rust standard library, and other libraries can easily inter-operate with GrizzlySSL.

This all means that GrizzlySSL can provide a I/O experience that is (essentially) no different from [`std::io`], even in `no_std` environments.

## Compile-time Parsing

It can often be more desirable to embed the necessary cryptographic files (root CAs, client certificates, etc.) into the application at compile-time.
To make this easier, the `grizzly-ssl-parsers` crate is provided. Simply add this crate as a `build-dependency` in the project's `Cargo.toml` file,
and use the provided methods as needed.

For a more detailed explaination, see the [crate documentation][1].

## Mozilla Root CA Store

Mozilla keeps a store of root CAs, containing hundreds of certificates. The `grizzly-ssl-certs` crate automatically generates an array of trust anchors
for use with GrizzlySSL, based off of the Mozilla store. This is useful for connecting to public services, as it is almost guarunteed that the root CA
that is being used is contained within the store.

To use it, simply include the `grizzly-ssl-certs` crate, and use `grizzly_ssl_certs::MOZILLA_ROOT_STORE` as the list of trust anchors.

## Feature Flags

Below is a list of Cargo features that can be used to customise GrizzlySSL.

- `alloc` - Use the `alloc` crate, allowing usage of heap-allocated objects.
- `std` - Use the standard library, allowing usage of heap-allocated objects, and the [`std::io`] module (see [I/O] for more info).

## License

Licensed under either of

 * Apache License, Version 2.0
   ([LICENSE-APACHE](LICENSE-APACHE) or http://www.apache.org/licenses/LICENSE-2.0)
 * MIT license
   ([LICENSE-MIT](LICENSE-MIT) or http://opensource.org/licenses/MIT)

at your option.

## Contribution

Unless you explicitly state otherwise, any contribution intentionally submitted
for inclusion in the work by you, as defined in the Apache-2.0 license, shall be
dual licensed as above, without any additional terms or conditions.

[1]: https://docs.rs/grizzly-ssl-parsers
[`bare-io`]: https://github.com/bbqsrc/bare-io
[`std::io`]: https://doc.rust-lang.org/nightly/std/io/
[`std::io::Error`]: https://doc.rust-lang.org/nightly/std/io/struct.Error.html
[`bare_io::Read`]: https://docs.rs/bare-io/*/bare_io/trait.Write.html
[`bare_io::Write`]: https://docs.rs/bare-io/*/bare_io/trait.Write.html
[I/O]: #io
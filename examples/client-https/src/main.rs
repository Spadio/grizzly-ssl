use grizzly_ssl::client::ClientConfig;
use grizzly_ssl::session::{Session, SessionExt};
use grizzly_ssl::{nb, EntropySource, GRIZZLY_BUFFER_SIZE};
use std::io::{ErrorKind, Read, Write};
use std::net::TcpStream;
use std::time::SystemTime;

fn main() {
	// Create a `ClientSession` for connecting to "https://example.com".
	let mut client = ClientConfig::new()
		.current_time(SystemTime::now())
		.entropy_source(EntropySource::OsImpl)
		.trust_anchors(grizzly_ssl_certs::MOZILLA_ROOT_STORE)
		.hostname(Some("example.com"))
		.start(vec![0; GRIZZLY_BUFFER_SIZE])
		.expect("Could not create ClientStream");

	// Now we need to create a socket. Whatever I/O object holds the raw TLS data is seperate from
	// the client, so we can create the socket after we have created our client.
	let mut socket = TcpStream::connect(("example.com", 443)).expect("Could not create TcpStream");

	// Although the I/O object does not strictly have to be non-blocking, it works best if it is,
	// as if the code needs to block, it can be done outside of the library code.
	socket.set_nonblocking(true).unwrap();

	// Keep performing the necessary I/O calls until the handshake is complete.
	while client.is_handshaking() {
		// GrizzlySSL uses `nb` to handle (non-)blocking I/O calls. This allows I/O objects to report
		// a "would block" error, and that is deferred to the caller. Simply put, this means you decide
		// how to handle blocking.
		//
		// Here, we use the `nb::block!` macro, which turns the non-blocking `ClientSession::perform_io`
		// (note that it is only non-blocking if the I/O object is) into a blocking operation.
		nb::block!(client.perform_io(&mut socket)).expect("Failed to perform I/O on ClientSession");
	}

	// Next, write a basic HTTP GET header.
	client
		.write_all(b"GET / HTTP/1.0\r\nHost: example.com\r\n\r\n")
		.expect("Could not write to ClientSession");
	// Now flush the buffered data onto the network.
	client
		.flush_raw(&mut socket)
		.expect("Could not flush ClientSession");

	// Perform the I/O operations in a loop, until either the socket is closed (`bytes_read` == Some(0)),
	// the client is closed (`grizzly_ssl::Error::Closed`), or an error occurs.
	loop {
		let bytes_read = match nb::block!(client.perform_io(&mut socket)) {
			Ok((_, bytes_read)) => bytes_read,
			Err(grizzly_ssl::Error::Closed) => break,
			Err(e) => panic!(e),
		};

		// Print out any buffered data.
		let mut buf = [0; 512];
		match client.read(&mut buf) {
			Ok(len) if len > 0 => print!("{}", String::from_utf8_lossy(&buf[..len])),
			// If the socket has closed, and we have printed all of the buffered data,
			// break out of the loop.
			Ok(_) if bytes_read == Some(0) => break,
			Ok(_) => (),
			Err(ref e) if e.kind() == ErrorKind::WouldBlock => (),
			Err(e) => panic!(e),
		}
	}
}

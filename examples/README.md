# Examples

## client-https

A basic client, using `std::net::TcpStream` and a `ClientSession` to connect to [https://example.com](https://example.com).
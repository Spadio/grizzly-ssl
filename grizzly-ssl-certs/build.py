# Based off of https://raw.githubusercontent.com/ctz/webpki-roots/main/build.py

import itertools
import cryptography.x509
from cryptography.hazmat.backends.openssl.backend import backend
from cryptography.hazmat.primitives import serialization
from cryptography.hazmat.primitives.asymmetric import ec, rsa
import urllib.parse
import urllib.request

ca_blacklist = ["Buypass Class 2 CA 1", "China Internet Network Information Center", "CNNIC",
                "RSA Security 2048 v3", "Root CA Generalitat Valenciana", "StartCom", "WoSign", "TÜRKTRUST", "TURKTRUST"]


def fetch_certs():
    url = "http://mkcert.org/generate/all/except/{0}".format(
        "+".join([urllib.parse.quote(x) for x in ca_blacklist]))
    contents = urllib.request.urlopen(url).read()
    return contents.decode("utf-8")


def split_certs(certs):
    cert = ""
    for line in certs.splitlines():
        if line.strip() != "":
            cert += "{0}\n".format(line)
        if "-----END CERTIFICATE-----" in line:
            yield cert
            cert = ""


def decode_cert(cert):
    decoded = cryptography.x509.load_pem_x509_certificate(bytes(cert, "utf-8"))
    return decoded


def grouper(iterable, n, fillvalue=None):
    args = [iter(iterable)] * n
    return itertools.zip_longest(*args, fillvalue=fillvalue)


def format_array(array, indent_level):
    output = "&["
    array = bytearray(array)

    if len(array) > 14:
        output += "\n"
        for elems in grouper(array, 14):
            elems = [x for x in elems if x is not None]
            output += "{0}{1},\n".format(
                "\t" * indent_level,
                ", ".join([f"{elem:#0{4}x}" for elem in elems]))

        output += "{0}]".format("\t" * (indent_level - 1))
    else:
        output += "{0}],".format(
            ", ".join([f"{elem:#0{4}x}" for elem in array]))
    return output


def generate_code(decoded):
    comment = decoded.issuer.rfc4514_string()
    dn = decoded.issuer.public_bytes()

    key_usage = decoded.extensions.get_extension_for_oid(
        cryptography.x509.BasicConstraints.oid).value

    if key_usage.ca:
        is_ca = "true"
    else:
        is_ca = "false"

    public_key = decoded.public_key()
    if isinstance(public_key, rsa.RSAPublicKey):
        nums = public_key.public_numbers()
        public_key_enum = "grizzly_ssl::x509::PublicKey::Rsa {{\n\t\t\tn: {0},\n\t\t\te: {1}\n\t\t}}".format(
            format_array(nums.n.to_bytes(
                public_key.key_size // 8, byteorder="big"), 4),
            format_array(nums.e.to_bytes(
                (nums.e.bit_length() + 7) // 8, byteorder="big"), 4)
        )
    elif isinstance(public_key, ec.EllipticCurvePublicKey):
        if isinstance(public_key.curve, ec.SECP192R1):
            nid = 409
        elif isinstance(public_key.curve, ec.SECP256R1):
            nid = 415
        elif isinstance(public_key.curve, ec.SECP224R1):
            nid = 713
        elif isinstance(public_key.curve, ec.SECP384R1):
            nid = 715
        elif isinstance(public_key.curve, ec.SECP521R1):
            nid = 716
        elif isinstance(public_key.curve, ec.SECP256K1):
            nid = 714
        elif isinstance(public_key.curve, ec.SECT163K1):
            nid = 721
        elif isinstance(public_key.curve, ec.SECT233K1):
            nid = 726
        elif isinstance(public_key.curve, ec.SECT283K1):
            nid = 729
        elif isinstance(public_key.curve, ec.SECT409K1):
            nid = 731
        elif isinstance(public_key.curve, ec.SECT571K1):
            nid = 733
        elif isinstance(public_key.curve, ec.SECT163R2):
            nid = 723
        elif isinstance(public_key.curve, ec.SECT233R1):
            nid = 727
        elif isinstance(public_key.curve, ec.SECT283R1):
            nid = 730
        elif isinstance(public_key.curve, ec.SECT409R1):
            nid = 732
        elif isinstance(public_key.curve, ec.SECT571R1):
            nid = 734
        elif isinstance(public_key.curve, ec.BrainpoolP256R1):
            nid = 927
        elif isinstance(public_key.curve, ec.BrainpoolP384R1):
            nid = 931
        elif isinstance(public_key.curve, ec.BrainpoolP512R1):
            nid = 933

        nums = public_key.public_numbers()
        public_key_enum = "grizzly_ssl::x509::PublicKey::Ec {{\n\t\t\tcurve: {0},\n\t\t\tq: {1}\n\t\t}}".format(
            str(nid),
            format_array(public_key.public_bytes(
                serialization.Encoding.X962, serialization.PublicFormat.UncompressedPoint), 4)
        )
    else:
        raise SyntaxError(decoded.public_key())

    output = "\t// {0}\n\tgrizzly_ssl::x509::TrustAnchor::new(\n\t\t{1},\n\t\t{2}, \n\t\t{3}\n\t),\n".format(
        comment,
        format_array(dn, 3),
        is_ca,
        public_key_enum
    )
    return output


if __name__ == "__main__":
    out_file = open("src/lib.rs", "w", encoding="utf-8")
    out_file.write(
        "/// Automatically generated from the Mozilla Root CA store. For more info, see https://gitlab.com/Spadio/grizzly-ssl/README.md#mozilla-root-ca-store\n\n")
    out_file.write(
        "pub static MOZILLA_ROOT_STORE: &[grizzly_ssl::x509::TrustAnchor] = &[\n")

    certs = fetch_certs()
    for cert in split_certs(certs):
        decoded = decode_cert(cert)
        out_file.write(generate_code(decoded))

    out_file.write("];")

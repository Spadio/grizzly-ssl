use std::fs::File;
use std::io::Read;
use std::path::PathBuf;

struct SourceFiles {
	core_source_files: Vec<PathBuf>,
}

impl SourceFiles {
	fn parse_files_variable(content: &str, var_name: &str) -> Vec<PathBuf> {
		let start_substring = format!("{}=\" \\\n", var_name);
		let start = content
			.find(&start_substring)
			.expect("Could not parse `vendor/mk/mkrules.sh`")
			+ start_substring.len();
		let content = content
			.get(start..)
			.expect("Could not parse `vendor/mk/mkrules.sh`");
		let raw_files = content
			.split('"')
			.next()
			.expect("Could not parse `vendor/mk/mkrules.sh`");

		raw_files
			.lines()
			.map(|line| line.trim_start().trim_end_matches(" \\"))
			.map(|line| format!("vendor/{}", line))
			.map(PathBuf::from)
			.collect()
	}

	pub fn get() -> Self {
		let mut f =
			File::open("vendor/mk/mkrules.sh").expect("Could not open `vendor/mk/mkrules.sh`");
		let mut s = String::new();
		f.read_to_string(&mut s)
			.expect("Could not read from `vendor/mk/mkrules.sh`");

		let core_source_files = Self::parse_files_variable(&s, "coresrc");

		Self { core_source_files }
	}
}

fn main() {
	// Get the BearSSL source files from the `mk/mkrules.sh` file, which generates the Makefile for BearSSL.
	let source_files = SourceFiles::get();

	// Tell cargo to rerun if any of the BearSSL source files change.
	for file in source_files.core_source_files.iter() {
		println!("cargo:rerun-if-changed={}", file.to_str().unwrap());
	}

	// Build the BearSSL library.
	cc::Build::new()
		.files(source_files.core_source_files)
		.define("BR_USE_UNIX_TIME", "0")
		.define("BR_USE_WIN32_TIME", "0")
		.static_flag(true)
		.include("vendor/inc")
		.include("vendor/src")
		.compile("bearssl");

	// Generate the `bindgen` bindings.
	let bindings = bindgen::Builder::default()
		.header("vendor/inc/bearssl.h")
		.use_core()
		.ctypes_prefix("cty")
		.parse_callbacks(Box::new(bindgen::CargoCallbacks))
		.generate()
		.expect("Unable to generate bindings");

	// Write the bindings to a file.
	let out_path = PathBuf::from(std::env::var("OUT_DIR").unwrap());
	bindings
		.write_to_file(out_path.join("bindings.rs"))
		.expect("Could not write bindings");
}

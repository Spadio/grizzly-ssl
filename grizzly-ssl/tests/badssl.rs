use grizzly_ssl::client::ClientConfig;
use grizzly_ssl::session::{Session, SessionExt};
use grizzly_ssl::{EntropySource, GRIZZLY_BUFFER_SIZE};
use grizzly_ssl_certs::MOZILLA_ROOT_STORE;
use std::net::TcpStream;
use std::time::SystemTime;

#[test]
fn name() {}

#[cfg(feature = "std")]
fn test_tls_connection(hostname: &str, port: u16) {
	use std::io::{Read, Write};

	let mut client = ClientConfig::new()
		.current_time(SystemTime::now())
		.entropy_source(EntropySource::OsImpl)
		.trust_anchors(MOZILLA_ROOT_STORE)
		.hostname(Some(hostname))
		.start(vec![0; GRIZZLY_BUFFER_SIZE])
		.unwrap();

	let mut socket = TcpStream::connect(format!("{}:{}", hostname, port)).unwrap();
	socket.set_nonblocking(true).unwrap();

	while client.is_handshaking() {
		nb::block!(client.perform_io(&mut socket)).unwrap();
	}

	client
		.write_all(format!("GET / HTTP/1.0\r\nHost: {}\r\n\r\n", hostname).as_bytes())
		.unwrap();
	client.flush_raw(&mut socket).unwrap();

	loop {
		let bytes_read = match nb::block!(client.perform_io(&mut socket)) {
			Ok((_, bytes_read)) => bytes_read,
			Err(grizzly_ssl::Error::Closed) => break,
			Err(e) => panic!(e),
		};

		let mut buf = [0; 512];
		match client.read(&mut buf) {
			Ok(len) if len > 0 => print!("{}", String::from_utf8_lossy(&buf[..len])),
			Ok(_) if bytes_read == Some(0) => break,
			Ok(_) => (),
			Err(ref e) if e.kind() == core2::io::ErrorKind::WouldBlock => (),
			Err(e) => panic!(e),
		}
	}
}

macro_rules! define_test {
	( $test_name:ident, $hostname:literal, $port:literal ) => {
		#[test]
		#[cfg(feature = "std")]
		fn $test_name() {
			const HOSTNAME: &str = $hostname;
			const PORT: u16 = $port;

			test_tls_connection(&HOSTNAME, PORT);
		}
	};
	( #[$test_attr:meta] $test_name:ident, $hostname:literal, $port:literal ) => {
		#[test]
		#[$test_attr]
		#[cfg(feature = "std")]
		fn $test_name() {
			const HOSTNAME: &str = $hostname;
			const PORT: u16 = $port;

			test_tls_connection(&HOSTNAME, PORT);
		}
	};
}

define_test!(
	#[should_panic(
		expected = "called `Result::unwrap()` on an `Err` value: EngineFailure(X509Expired)"
	)]
	badssl_expired,
	"expired.badssl.com",
	443
);
define_test!(
	#[should_panic(
		expected = "called `Result::unwrap()` on an `Err` value: EngineFailure(X509BadServerName)"
	)]
	badssl_wrong_host,
	"wrong.host.badssl.com",
	443
);
define_test!(
	#[should_panic(
		expected = "called `Result::unwrap()` on an `Err` value: EngineFailure(X509NotTrusted)"
	)]
	badssl_self_signed,
	"self-signed.badssl.com",
	443
);
define_test!(
	#[should_panic(
		expected = "called `Result::unwrap()` on an `Err` value: EngineFailure(X509NotTrusted)"
	)]
	badssl_untrusted_root,
	"untrusted-root.badssl.com",
	443
);
// These two should fail ideally, but don't with BearSSL.
define_test!(badssl_revoked, "revoked.badssl.com", 443);
define_test!(badssl_pinning_test, "pinning-test.badssl.com", 443);

define_test!(badssl_tls_v1_0, "tls-v1-0.badssl.com", 1010);
define_test!(badssl_tls_v1_1, "tls-v1-1.badssl.com", 1011);
define_test!(badssl_tls_v1_2, "tls-v1-2.badssl.com", 1012);

define_test!(badssl_cbc, "cbc.badssl.com", 443);
define_test!(badssl_3des, "3des.badssl.com", 443);
define_test!(
	#[should_panic(
		expected = "called `Result::unwrap()` on an `Err` value: EngineFailure(X509ExtraElement)"
	)]
	badssl_rc4_md5,
	"rc4-md5.badssl.com",
	443
);
define_test!(
	#[should_panic(
		expected = "called `Result::unwrap()` on an `Err` value: EngineFailure(X509ExtraElement)"
	)]
	badssl_rc4,
	"rc4.badssl.com",
	443
);
define_test!(
	#[should_panic(
		expected = "called `Result::unwrap()` on an `Err` value: EngineFailure(X509ExtraElement)"
	)]
	badssl_null,
	"null.badssl.com",
	443
);

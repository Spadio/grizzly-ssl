/// Defines ALPN protocol names at compile-time.
///
/// This macro makes it converts the given string literals into FFI compatible
/// strings, and defines them statically. This means the lifetime of the
/// strings is always `'static`, so the pointers provided to BearSSL will always
/// be valid.
///
/// # Example
///
/// ```
/// use grizzly_ssl::{AlpnProtocol, define_protocols};
///
/// static ALPN_PROTOCOLS: &[AlpnProtocol] = define_protocols!["protocol-foo", "protocol-bar"];
/// ```
#[macro_export]
macro_rules! define_protocols {
	[ $( $name:literal ),* ] => {
		&[
			$({
					static NAME: &[u8] = concat!($name, '\x00').as_bytes();
					$crate::AlpnProtocol(NAME.as_ptr())
			},)*
		]
	};
}

/// Newtype for ALPN protocol name.
///
/// This type should be created using the [`define_protocols!`] macro.
///
/// [`define_protocols!`]: macro.define_protocols.html
#[derive(Copy, Clone, Debug)]
pub struct AlpnProtocol(pub *const u8);

unsafe impl Send for AlpnProtocol {}
unsafe impl Sync for AlpnProtocol {}

// TODO: once const generics are stablized, allow an array of
// entropy bytes of any length.

/// A cryptographically secure source of random entropy, used for encryption.
#[derive(Debug)]
pub enum EntropySource {
	/// Use an OS-provided source of random entropy.
	#[cfg(all(feature = "std", any(unix, windows)))]
	OsImpl,
	/// Use a custom, user-provided entropy source.
	Custom([u8; 32]),
}

impl EntropySource {
	pub(crate) unsafe fn set_entropy(&self, ssl_engine: &mut bear_ssl_sys::br_ssl_engine_context) {
		match self {
			// If the target is a system supported by BearSSL,
			// then we don't need to do anything, as the library
			// will automatically configure the entropy for us in
			// the method `br_ssl_engine_init_rand()`.
			#[cfg(all(feature = "std", any(unix, windows)))]
			EntropySource::OsImpl => (),
			EntropySource::Custom(buf) => {
				bear_ssl_sys::br_ssl_engine_inject_entropy(
					ssl_engine as *mut _,
					buf.as_ptr() as *const _,
					buf.len() as bear_ssl_sys::size_t,
				);
			}
		}
	}
}

/// Supported TLS protocol versions.
///
/// This type is used to specify which TLS versions a session can support.
/// This is done by passing a range of type `ProtocolVersion` to the corresponding
/// configuration object. For an example, see [`ClientConfig::supported_versions`].
///
/// [`ClientConfig::supported_versions`]: client/struct.ClientConfig.html#method.supported_versions
#[derive(Copy, Clone, Debug, Eq, Ord, PartialEq, PartialOrd)]
pub enum ProtocolVersion {
	/// TLS v1.0
	TlsV1_0,
	/// TLS v1.1
	TlsV1_1,
	/// TLS v1.2
	TlsV1_2,
}

impl ProtocolVersion {
	pub(crate) fn to_br_version(&self) -> u16 {
		match self {
			ProtocolVersion::TlsV1_0 => bear_ssl_sys::BR_TLS10 as u16,
			ProtocolVersion::TlsV1_1 => bear_ssl_sys::BR_TLS11 as u16,
			ProtocolVersion::TlsV1_2 => bear_ssl_sys::BR_TLS12 as u16,
		}
	}
}

/// A representation of the current date and time.
///
/// This type can be constructed by providing one of:
/// - Days and seconds since January 1st, 0AD ([`from_days_seconds`])
/// - A [`std::time::SystemTime`], using [`From<SystemTime>`][1] (with the `std` feature enabled)
///
/// [`from_days_seconds`]: #method.from_days_seconds
/// [1]: https://doc.rust-lang.org/nightly/std/convert/trait.From.html
#[derive(Copy, Clone, Debug, PartialEq)]
pub struct CurrentDateTime {
	pub(crate) days: u32,
	pub(crate) seconds: u32,
}

impl CurrentDateTime {
	/// Construct a timestamp from the `days` and `seconds` elapsed since January 1st, 0AD (1BC).
	pub const fn from_days_seconds(days: u32, seconds: u32) -> Self {
		Self { days, seconds }
	}
}

#[cfg(feature = "std")]
impl From<std::time::SystemTime> for CurrentDateTime {
	fn from(instant: std::time::SystemTime) -> Self {
		// We will ignore leap seconds.
		let epoch_secs = match instant.duration_since(std::time::UNIX_EPOCH) {
			Ok(dur) => dur.as_secs() as i64,
			Err(e) => -(e.duration().as_secs() as i64),
		};
		let epoch_days = epoch_secs / 86_400;

		// 719,528 days elapsed between Jan. 1st, 0AD and Jan. 1st, 1970.
		let days = epoch_days + 719_528;
		let secs = epoch_secs % 86_400;

		Self {
			days: days as u32,
			seconds: secs as u32,
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	fn alpn_protocol_define() {
		let protocols = define_protocols!["Hello", "World"];
		unsafe {
			assert_eq!(std::slice::from_raw_parts(protocols[0].0, 6), b"Hello\x00");
			assert_eq!(std::slice::from_raw_parts(protocols[1].0, 6), b"World\x00");
		}
	}

	#[test]
	#[cfg(feature = "std")]
	fn currentdatetime_from_systemtime() {
		use std::time::SystemTime;

		assert_eq!(
			CurrentDateTime::from(SystemTime::UNIX_EPOCH),
			CurrentDateTime {
				days: 719_528,
				seconds: 0
			}
		);
	}
}

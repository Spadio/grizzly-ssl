//! GrizzlySSL is a idiomatic Rust wrapper for [BearSSL](https://bearssl.org/index.html).
//!
//! ## Overview
//!
//! BearSSL is an implementation of the SSL/TLS protocol (RFC 5246) written in C. It aims at offering the following features:
//! - Be correct and secure. In particular, insecure protocol versions and choices of algorithms are not supported, by design; cryptographic algorithm implementations are constant-time by default.
//! - Be small, both in RAM and code footprint. For instance, a minimal server implementation may fit in about 20 kilobytes of compiled code and 25 kilobytes of RAM.
//! - Be highly portable. BearSSL targets not only “big” operating systems like Linux and Windows, but also small embedded systems and even special contexts like bootstrap code.
//! - Be feature-rich and extensible. SSL/TLS has many defined cipher suites and extensions; BearSSL should implement most of them, and allow extra algorithm implementations to be added afterwards, possibly from third parties.
//!
//! For more information, see the [BearSSL website][1].
//!
//! ## Supported BearSSL Features
//!
//! - No dynamic allocation required.
//! - TLS 1.0, TLS 1.1 and TLS 1.2 support.
//! - RSA, ECDH and ECDHE key exchange support.
//! - [*Minimal*][2] X.509 certificate validation.
//! - Client certificate support.
//! - AES/GCM, AES/CCM, AES/CBC, 3DES/CBC and ChaCha20+Poly1305 support.
//! - Constant-time encryption for immunity to side-channel attacks.
//! - [Maximum Fragment Length][3] extension support.
//! - [Application-Layer Protocol Negotiation (ALPN)][4] extension support.
//!
//! ### Minimal X.509 Implementation Details
//!
//! While BearSSL does not support all the bells and whistles of X.509 validation, it still enforces the basics:
//! subject/issuer DN matching, notBefore/notAfter dates, basic constraints, and key usage extension.
//! Optionally, the client will also verify that the server's certificate contains the intended dNSName
//! (in the Subject Alt Name extension, or in the Common Name if the SAN extension is missing);
//! this check can handle a "wildcard" at the start of a name.
//!
//! RSA and ECDSA signatures are supported, along with all of the "classical" hash functions (SHA-1, and the SHA-2
//! family, from SHA-224 to SHA-512). Note that MD5 is voluntarily excluded.
//!
//! The validation engine "fails safe", in that it rejects any certificate that triggers an unsupported feature
//! in a critical extension. However, it must be noted that the minimal engine does not implement revocation checks.
//!
//! ## Feature Flags
//!
//! - `alloc` - Use the `alloc` crate, to allow usage of heap-allocated objects.
//! - `std` - Use the standard library, enabling heap-allocated objects, and the [`std::io`] module (see [I/O] for more info).
//!
//! ## I/O
//!
//! GrizzlySSL uses the [`bare-io`] library for handling I/O, which acts like the Rust standard library [`std::io`]
//! module (and with the `std` feature flag, is essentially just an alias to it), but changes the [`std::io::Error`]
//! type so that it can be used in `no_std` environments.
//!
//! This means that the commonly used interface from the standard library can be used in GrizzlySSL, and with the `std` feature flag,
//! the types that implement [`bare_io::Read`] and [`bare_io::Write`] implement the [`std::io`] version of those traits (instead of the [`bare-io`] versions),
//! so the Rust standard library, and other libraries can easily inter-operate with GrizzlySSL.
//!
//! This all means that GrizzlySSL can provide a I/O experience that is (essentially) no different from [`std::io`], even in `no_std` environments.
//!
//! ## Sessions
//!
//! Each seperate instance of a TLS connection/stream is classified as a "session". Each different kind of session implements the [`Session`] trait,
//! providing a unified interface for dealing with these sessions.
//!
//! The trait documentation goes into detail about the state machine API, which is used internally, and how to interact with it.
//!
//! ## Parsers
//!
//! For many use cases (mostly in embedded systems), any certificates, or private keys, will need to be baked into the application at compile-time.
//! To help facilitate this, the [`grizzly-ssl-parsers`] crate exists. See the crate documentation for more information.
//!
//! ## Examples
//!
//! For examples, see the `examples` directory in the source repository.
//!
//! [1]: https://bearssl.org/index.html
//! [2]: #minimal-x509-implementation-details
//! [3]: https://tools.ietf.org/html/rfc6066#section-4
//! [4]: https://tools.ietf.org/html/rfc7301
//! [`std::io`]: https://doc.rust-lang.org/nightly/std/io/
//! [I/O]: #io
//! [`bare-io`]: ../bare_io/index.html
//! [`std::io::Error`]: https://doc.rust-lang.org/nightly/std/io/struct.Error.html
//! [`bare_io::Read`]: ../bare_io/trait.Write.html
//! [`bare_io::Write`]: ../bare_io/trait.Write.html
//! [`Session`]: session/trait.Session.html
//! [`grizzly-ssl-parsers`]: https://docs.rs/grizzly-ssl-parsers
#![cfg_attr(not(any(test, feature = "std")), no_std)]

pub use bare_io;
pub use nb;

pub mod client;
pub mod error;
pub use error::Error;
pub mod session;
mod types;
pub use types::*;
pub mod x509;

pub(crate) type Result<T> = core::result::Result<T, Error>;

/// The ideal GrizzlySSL I/O buffer size.
///
/// A smaller buffer can be provided, and the library will adapt. In this case,
/// the input buffer will be favoured, so as to be able to support all records sent
/// by the peer.
///
/// If the buffer provided is less than `16709` bytes, then the library, *as a client*,
/// will use the [Maximum Fragment Length Negotiation extension][1], in an attempt to make the
/// server agree not to use larger records.
///
/// Unfortunately, the extension is asymmetrical, in that it is for the client only;
/// a RAM-constrained server has no standard way to announce that it cannot handle full-sized records.
/// Moreover, while BearSSL, as a server, recognises and honours that extension, not all existing,
/// deployed SSL server implementations do so.
/// Therefore, using smaller buffers in heterogeneous system deployments is a kind of hit-and-miss game.
/// To some extent, an application can force its SSL library to emit short records by making flush calls,
/// but this is fragile.
///
/// [1]: https://tools.ietf.org/html/rfc6066#section-4
pub const GRIZZLY_BUFFER_SIZE: usize = bear_ssl_sys::BR_SSL_BUFSIZE_BIDI as usize;

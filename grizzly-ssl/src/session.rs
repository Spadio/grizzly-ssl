//! TLS session traits.
//!
//! A [`Session`] represents a TLS state machine for a single connection. For more information,
//! view the documentaton for the [`Session`] trait.
//!
//! [`SessionExt`] contains extra helper methods for any type implementing [`Session`].
//!
//! [`Session`]: trait.Session.html
//! [`SessionExt`]: trait.SessionExt.html

use crate::error::Error;
use bare_io::{Read, Write};

/// A trait which represents a TLS session.
///
/// A TLS session works as a state machine - meaning it is completely seperate
/// from any I/O calls or objects. This allows for a lot of flexibility in how
/// a session can be implemented; threading is not required, meaning that for
/// resource constrained targets, no RAM needs to be wasted in maintaining
/// multiple stacks.
///
/// As the diagram below illustrates, raw TLS data is read/written using the [`read_raw`]
/// and [`write_raw`] methods, and the `Session` object converts them into plaintext,
/// which is accessed using the [`read`] and [`write`] methods.
///
/// ```text
///    TLS                 Plaintext
/// ==========             ==========
///
/// read_raw()             read()
/// ---------> +---------+ --------->
///            | Session |
/// <--------- +---------+ <---------
/// write_raw()            write()
/// ```
///
/// [`read_raw`]: #tymethod.read_raw
/// [`write_raw`]: #tymethod.write_raw
/// [`read`]: ../../bare_io/trait.Read.html#tymethod.read
/// [`write`]: ../../bare_io/trait.Write.html#tymethod.write
pub trait Session: Read + Write {
	/// Checks if the session is still alive.
	///
	/// Some SSL implentations may not send a `close_notify`, but just close the corresponding socket;
	/// this means that just because this method returns `true`, it does not necessarily mean the peer
	/// has not dropped the connection.
	fn is_alive(&self) -> bool;

	/// Check if the handshaking process has been completed.
	fn is_handshaking(&self) -> bool;

	/// Gets the ALPN protocol name.
	///
	/// If the session is still handshaking, or if the client and server could not find a common protocol,
	/// this method will return `None`.
	fn selected_protocol(&self) -> Option<&str>;

	/// Check if the client wants [`read_raw`] to be called.
	///
	/// [`read_raw`]: #tymethod.read_raw
	fn wants_read(&self) -> bool;

	/// Check if the client wants [`write_raw`] to be called.
	///
	/// [`write_raw`]: #tymethod.write_raw
	fn wants_write(&self) -> bool;

	/// Read raw TLS data from `io`, returning the amount of bytes recieved.
	///
	/// The incoming data is buffered, so the data can be read in
	/// arbitrary-sized chunks (such as from a socket, or a pipe).
	///
	/// An error is returned if, either, `io` returned an error when attempting to
	/// read from it, or if an error occurred while processing an new record.
	///
	/// If, after new data has been recieved, the buffer contains a full record,
	/// the engine will automatically process it, and handle it accordingly.
	///
	/// This method returns [`nb::Error::WouldBlock`] if `io` returns an error of kind [`bare_io::Error::WouldBlock`].
	///
	/// [`nb::Error::WouldBlock`]: ../../nb/enum.Error.html#variant.WouldBlock
	/// [`bare_io::Error::WouldBlock`]: ../../bare_io/enum.ErrorKind.html#variant.WouldBlock
	fn read_raw(&mut self, io: impl Read) -> nb::Result<usize, Error>;

	/// Write any buffered raw TLS data to `io`, returning the amount of bytes written.
	///
	/// An error is returned if, either, `io` returned an error when attempting to
	/// read from it, or if an error occurred while processing an new record.
	///
	/// This method returns [`nb::Error::WouldBlock`] if `io` returns an error of kind [`bare_io::Error::WouldBlock`].
	///
	/// [`nb::Error::WouldBlock`]: ../../nb/enum.Error.html#variant.WouldBlock
	/// [`bare_io::Error::WouldBlock`]: ../../bare_io/enum.ErrorKind.html#variant.WouldBlock
	fn write_raw(&mut self, io: impl Write) -> nb::Result<usize, Error>;
}

/// Extra methods for implementations of [`Session`].
///
/// [`Session`]: trait.Session.html
pub trait SessionExt: Session {
	/// Flush any buffered plaintext data, so that the engine turns it into TLS records,
	/// and then write it to `io`.
	///
	/// Returns the amount of bytes written.
	fn flush_raw(&mut self, mut io: impl Write) -> nb::Result<usize, Error> {
		self.flush().map_err(Error::Io)?;

		let mut bytes_written = 0;
		while self.wants_write() {
			bytes_written += self.write_raw(&mut io)?;
		}
		Ok(bytes_written)
	}

	/// Perform any pending I/O operations.
	///
	/// This means:
	/// - [Flush any buffered plaintext data][1] to `io`.
	/// - If the sessions [`wants_read`], then call [`read_raw`] once.
	///
	/// This method returns `(bytes_written, bytes_read)`, where `bytes_read` is `None` if [`read_raw`] returned
	/// [`nb::Error::WouldBlock`]. This is so the function does not hang indefinitely if `io` would block while reading.
	///
	/// [1]: #tymethod.flush_raw
	/// [`wants_read`]: trait.Session.html#tymethod.wants_read
	/// [`read_raw`]: trait.Session.html#tymethod.read_raw
	/// [`nb::Error::WouldBlock`]: ../../nb/enum.Error.html#variant.WouldBlock
	fn perform_io(
		&mut self,
		mut io: impl Read + Write,
	) -> nb::Result<(usize, Option<usize>), Error> {
		let bytes_written = self.flush_raw(&mut io)?;
		let bytes_read = match self.read_raw(&mut io) {
			Ok(bytes_read) => Some(bytes_read),
			Err(nb::Error::WouldBlock) => None,
			Err(e) => return Err(e),
		};

		Ok((bytes_written, bytes_read))
	}
}

impl<T: Session> SessionExt for T {}

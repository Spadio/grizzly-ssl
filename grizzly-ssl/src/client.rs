//! Connecting to a remote server as a TLS client.
//!
//! This module provides types to handle TLS client functionality.

#![allow(clippy::uninit_assumed_init)]

use crate::session::Session;
use crate::x509::{ClientCertificate, TrustAnchor};
use crate::{error::Error, AlpnProtocol, CurrentDateTime, EntropySource, ProtocolVersion, Result};
use bear_ssl_sys::{br_ssl_client_context, br_x509_minimal_context, cty::*, size_t};
use core::convert::TryInto;
use core::marker::PhantomData;
use core::mem::MaybeUninit;
use core::ops::{Bound, RangeBounds};
use core::ptr::NonNull;
use core2::io::{Read, Write};
use managed::ManagedSlice;

/// A client configuration builder, providing fine-grained control over how a [`ClientSession`] should be created.
///
/// A default configuration can be obtained by calling [`new`].
/// Additional configurations options can be changed by calling any of the builder methods.
///
/// Once the required fields have been configured, and all other wanted configuration options are set,
/// call [`start`] to create a [`ClientSession`].
///
/// # Required Fields
///
/// For the client to operate, it needs to have, at minimum:
/// - [An entropy source][2]
/// - [A list of trust anchors][3]
/// - [The current date and time][4]
///
/// Without these, calling [`start`] will always fail.
///
/// [1]: #required-fields
/// [2]: #method.entropy_source
/// [3]: #method.trust_anchors
/// [4]: #method.current_time
/// [`ClientSession`]: struct.ClientSession.html
/// [`new`]: #method.new
/// [`EntropySource`]: ../enum.EntropySource.html
/// [`start`]: #method.start
pub struct ClientConfig<'a> {
	alpn_protocols: Option<&'a [AlpnProtocol]>,
	client_certificate: Option<&'a ClientCertificate<'a>>,
	current_time: Option<CurrentDateTime>,
	entropy_source: Option<EntropySource>,
	hostname: Option<&'a str>,
	supported_versions: (ProtocolVersion, ProtocolVersion),
	trust_anchors: Option<&'a [TrustAnchor<'a>]>,
}

impl<'a> ClientConfig<'a> {
	/// Create a new config.
	pub fn new() -> Self {
		Self {
			alpn_protocols: None,
			client_certificate: None,
			current_time: None,
			entropy_source: None,
			hostname: None,
			supported_versions: (ProtocolVersion::TlsV1_0, ProtocolVersion::TlsV1_2),
			trust_anchors: None,
		}
	}

	/// Set the ALPN protocols that the client will use.
	///
	/// The `protocols` slice is most easily defined using the [`grizzly_ssl::define_protocols`] macro.
	///
	/// [`grizzly_ssl::define_protocols`]: ../macro.define_protocols.html
	pub fn alpn_protocols(&mut self, protocols: Option<&'a [AlpnProtocol]>) -> &mut Self {
		self.alpn_protocols = protocols;
		self
	}

	/// Set the client certificate the client will use.
	pub fn client_certificate(
		&mut self,
		client_certificate: Option<&'a ClientCertificate<'a>>,
	) -> &mut Self {
		self.client_certificate = client_certificate;
		self
	}

	/// Set the current time, so that the client is able to verify if X.509 certificates are expired.
	///
	/// This method can take a [`CurrentDateTime`], or a [`std::time::SystemTime`] as a parameter -
	/// see the documentation for [`CurrentDateTime`] for more information.
	///
	/// Note that the time does not update automatically, so care needs to be taken to ensure that
	/// the client is created not long after this method is called.
	///
	/// **This is a [required field](#required-field).**
	///
	/// # Example
	///
	/// ```
	/// use grizzly_ssl::client::ClientConfig;
	/// use std::time::SystemTime;
	///
	/// let mut config = ClientConfig::new();
	/// config.current_time(SystemTime::now());
	/// ```
	///
	/// [`CurrentDateTime`]: ../struct.CurrentDateTime.html
	pub fn current_time(&mut self, now: impl Into<CurrentDateTime>) -> &mut Self {
		self.current_time = Some(now.into());
		self
	}

	/// Set the cryptographically secure source of random entropy for the client.
	///
	/// **This is a [required field](#required-field).**
	pub fn entropy_source(&mut self, entropy_source: EntropySource) -> &mut Self {
		self.entropy_source = Some(entropy_source);
		self
	}

	/// Set the hostname to be used by [SNI (Server Name Indication)][1].
	///
	/// If a hostname is not provided, or is set to `None`, then the [SNI][1]
	/// extension will not be sent to the server.
	///
	/// [1]: https://tools.ietf.org/html/rfc3546#section-3.1
	pub fn hostname(&mut self, hostname: Option<&'a str>) -> &mut Self {
		self.hostname = hostname;
		self
	}

	/// Set the TLS versions that the client will accept.
	///
	/// The `versions` parameter accept any Rust `Range` type ([`Range`], [`RangeInclusive`], etc.).
	///
	/// # Example
	///
	/// ```
	/// use grizzly_ssl::client::ClientConfig;
	/// use grizzly_ssl::ProtocolVersion;
	///
	/// let mut config = ClientConfig::new();
	/// config.supported_versions(ProtocolVersion::TlsV1_0..=ProtocolVersion::TlsV1_2);
	/// ```
	pub fn supported_versions(&mut self, versions: impl RangeBounds<ProtocolVersion>) -> &mut Self {
		let start = match versions.start_bound() {
			Bound::Included(start) | Bound::Excluded(start) => *start,
			Bound::Unbounded => ProtocolVersion::TlsV1_0,
		};
		let end = match versions.end_bound() {
			Bound::Included(end) | Bound::Excluded(end) => *end,
			Bound::Unbounded => ProtocolVersion::TlsV1_2,
		};

		self.supported_versions = (start, end);
		self
	}

	/// Set the trust anchors that the client should use.
	///
	/// **This is a [required field](#required-field).**
	pub fn trust_anchors(&mut self, trust_anchors: &'a [TrustAnchor<'a>]) -> &mut Self {
		self.trust_anchors = Some(trust_anchors);
		self
	}

	/// Construct a new [`ClientSession`] from the provided configuration options, and an `io_buffer`.
	///
	/// # Errors
	///
	/// This method will fail if:
	/// - The provided `io_buffer` is less than `1349` bytes long.
	/// - The provided [`hostname`] is more than `255` bytes long.
	/// - Any [required field][1] is not set.
	///
	/// [1]: #required-fields
	/// [`ClientSession`]: struct.ClientSession.html
	/// [`hostname`]: #method.hostname
	pub fn start(&self, io_buffer: impl Into<ManagedSlice<'a, u8>>) -> Result<ClientSession<'a>> {
		let CurrentDateTime { days, seconds } = self.current_time.ok_or(Error::MissingRequired)?;
		let entropy_source = self.entropy_source.as_ref().ok_or(Error::MissingRequired)?;
		let trust_anchors = self.trust_anchors.as_ref().ok_or(Error::MissingRequired)?;

		unsafe {
			let mut io_buffer = io_buffer.into();
			if io_buffer.len() < 1349 {
				return Err(Error::InvalidParam);
			}

			// Create the BearSSL structs.
			let mut ssl_context = MaybeUninit::<br_ssl_client_context>::zeroed().assume_init();
			let mut x509_context = MaybeUninit::<br_x509_minimal_context>::uninit().assume_init();

			// Set the supported protocol versions.
			ssl_context.eng.version_min = self.supported_versions.0.to_br_version();
			ssl_context.eng.version_max = self.supported_versions.1.to_br_version();

			// TODO: add feature flags for configuring cipher suites and algorithm implementations.
			bear_ssl_sys::br_ssl_client_init_full(
				&mut ssl_context as *mut _,
				&mut x509_context as *mut _,
				trust_anchors.as_ptr() as *const _,
				trust_anchors.len() as bear_ssl_sys::size_t,
			);

			if let Some(client_certificate) = self.client_certificate {
				client_certificate.set_br_ssl_context(&mut ssl_context)?;
			}

			// Set entropy bytes.
			entropy_source.set_entropy(&mut ssl_context.eng);

			// Tell the engine where the I/O buffer is.
			bear_ssl_sys::br_ssl_engine_set_buffer(
				&mut ssl_context.eng as *mut _,
				io_buffer.as_mut_ptr() as *mut _,
				io_buffer.len() as bear_ssl_sys::size_t,
				1,
			);

			let status = {
				if let Some(hostname) = self.hostname {
					// Copy the hostname `str` into a `u8` array so that the hostname
					// ends with a null byte. The buffer is copied into the engine
					// context struct, so it only needs to live until we call
					// `br_ssl_client_reset`.
					let mut buf = [0; 256];
					let hostname_len = hostname.bytes().len();

					// Ensure the hostname length is < 256 bytes.
					if hostname_len > 255 {
						return Err(Error::InvalidParam);
					}

					// Copy the hostname into the buffer.
					buf.get_mut(0..hostname_len)
						.ok_or(Error::InvalidParam)?
						.copy_from_slice(hostname.as_bytes());

					// Reset the client context in preperation for a new handshake.
					// The provided target host name will be used for SNI.
					bear_ssl_sys::br_ssl_client_reset(
						&mut ssl_context as *mut _,
						buf.as_ptr() as *const _,
						0,
					)
				} else {
					// No hostname was provided, so don't send SNI.
					bear_ssl_sys::br_ssl_client_reset(
						&mut ssl_context as *mut _,
						core::ptr::null(),
						0,
					)
				}
			};

			if status == 0 {
				return Err(Error::from_engine(&ssl_context.eng));
			}

			// Set the ALPN protocol names.
			if let Some(protocols) = self.alpn_protocols {
				ssl_context.eng.protocol_names = protocols.as_ptr() as *mut _;
				ssl_context.eng.protocol_names_num = protocols
					.len()
					.try_into()
					.map_err(|_| Error::InvalidParam)?;
			}

			// Set the current time.
			x509_context.days = days;
			x509_context.seconds = seconds;

			Ok(ClientSession {
				handshake_completed: false,
				io_buffer,
				is_active: true,
				ssl_context,
				x509_context,
				_phantom: PhantomData,
			})
		}
	}
}

impl<'a> Default for ClientConfig<'a> {
	fn default() -> Self {
		Self::new()
	}
}

/// Representation of a single TLS client session.
///
/// This structure is used to represent, and manage, TLS client sessions. A client session is created
/// via the [`ClientConfig`] struct, which allows configuration via a builder-style interface.
///
/// # Methods
///
/// This structure implements the [`Session`] trait, which contains all of the methods for interacting
/// with a session.
///
/// [`ClientConfig`]: struct.ClientConfig.html
/// [`Session`]: ../session/trait.Session.html
#[allow(dead_code)]
pub struct ClientSession<'a> {
	handshake_completed: bool,
	io_buffer: ManagedSlice<'a, u8>,
	is_active: bool,
	ssl_context: br_ssl_client_context,
	x509_context: br_x509_minimal_context,
	_phantom: PhantomData<&'a [u8]>,
}

impl<'a> ClientSession<'a> {
	fn br_state(&self) -> c_uint {
		unsafe { bear_ssl_sys::br_ssl_engine_current_state(&self.ssl_context.eng as *const _) }
	}

	fn refresh_state(&mut self) -> Result<()> {
		let state = self.br_state();
		if state & bear_ssl_sys::BR_SSL_CLOSED != 0 {
			self.is_active = false;
			Err(Error::from_engine(&self.ssl_context.eng))
		} else if !self.handshake_completed && state & bear_ssl_sys::BR_SSL_SENDAPP != 0 {
			self.handshake_completed = true;
			Ok(())
		} else {
			Ok(())
		}
	}

	/// Destroy the TLS client session, and return the I/O buffer.
	pub fn close(self) -> ManagedSlice<'a, u8> {
		self.io_buffer
	}
}

unsafe impl<'a> Send for ClientSession<'a> {}

impl<'a> Session for ClientSession<'a> {
	fn is_alive(&self) -> bool {
		self.is_active
	}

	fn is_handshaking(&self) -> bool {
		self.is_active && !self.handshake_completed
	}

	fn selected_protocol(&self) -> Option<&str> {
		let selected = self.ssl_context.eng.selected_protocol;
		if selected != 0 && selected != 0xFFFF {
			unsafe {
				let ptr = self
					.ssl_context
					.eng
					.protocol_names
					.add((selected - 1).into()) as *const *const u8;
				let len = {
					let mut len = 0;
					let mut ptr = *ptr;
					while *ptr != b'\x00' {
						ptr = ptr.add(1);
						len += 1;
					}
					len
				};

				Some(core::str::from_utf8_unchecked(core::slice::from_raw_parts(
					*ptr, len,
				)))
			}
		} else {
			None
		}
	}

	fn wants_read(&self) -> bool {
		self.is_active && self.br_state() & bear_ssl_sys::BR_SSL_RECVREC != 0
	}

	fn wants_write(&self) -> bool {
		self.is_active && self.br_state() & bear_ssl_sys::BR_SSL_SENDREC != 0
	}

	fn read_raw(&mut self, mut io: impl Read) -> nb::Result<usize, Error> {
		unsafe {
			if !self.is_active || !self.wants_read() {
				return Ok(0);
			}

			let mut len: size_t = 0;
			let buf = NonNull::new(bear_ssl_sys::br_ssl_engine_recvrec_buf(
				&mut self.ssl_context.eng as *mut _,
				&mut len as *mut _,
			))
			.unwrap();

			match io.read(core::slice::from_raw_parts_mut(buf.as_ptr(), len as usize)) {
				Ok(rlen) => {
					// If we recieved some data, inform the engine.
					if rlen > 0 {
						bear_ssl_sys::br_ssl_engine_recvrec_ack(
							&mut self.ssl_context.eng as *mut _,
							rlen as size_t,
						);
					}

					self.refresh_state()?;
					Ok(rlen)
				}
				Err(ref e) if e.kind() == core2::io::ErrorKind::WouldBlock => {
					Err(nb::Error::WouldBlock)
				}
				Err(e) => Err(nb::Error::Other(Error::Io(e))),
			}
		}
	}

	fn write_raw(&mut self, mut io: impl Write) -> nb::Result<usize, Error> {
		unsafe {
			if !self.wants_write() {
				return Ok(0);
			}

			let mut len: size_t = 0;
			let buf = NonNull::new(bear_ssl_sys::br_ssl_engine_sendrec_buf(
				&mut self.ssl_context.eng as *mut _,
				&mut len as *mut _,
			))
			.unwrap();

			match io.write(core::slice::from_raw_parts(buf.as_ptr(), len as usize)) {
				Ok(wlen) => {
					// If we write some data, inform the engine.
					if wlen > 0 {
						bear_ssl_sys::br_ssl_engine_sendrec_ack(
							&mut self.ssl_context.eng as *mut _,
							wlen as size_t,
						);
					}

					self.refresh_state()?;
					Ok(wlen)
				}
				Err(ref e) if e.kind() == core2::io::ErrorKind::WouldBlock => {
					Err(nb::Error::WouldBlock)
				}
				Err(e) => Err(nb::Error::Other(Error::Io(e))),
			}
		}
	}
}

impl<'a> core2::io::Read for ClientSession<'a> {
	/// If the client has no data to read, an error with kind [`core2::io::Error::WouldBlock`] will be returned.
	///
	/// [`core2::io::Error::WouldBlock`]: ../../core2/enum.ErrorKind.html#variant.WouldBlock
	fn read(&mut self, buf: &mut [u8]) -> core2::io::Result<usize> {
		if self.br_state() & bear_ssl_sys::BR_SSL_RECVAPP == 0 {
			return Err(core2::io::Error::from(core2::io::ErrorKind::WouldBlock));
		} else if buf.is_empty() {
			return Ok(0);
		}

		unsafe {
			let mut len = 0 as bear_ssl_sys::size_t;
			let src = NonNull::new(bear_ssl_sys::br_ssl_engine_recvapp_buf(
				&mut self.ssl_context.eng as *mut _,
				&mut len as *mut _,
			))
			.unwrap();

			let len = core::cmp::min(len as usize, buf.len());
			let src = core::slice::from_raw_parts_mut(src.as_ptr(), len);
			// Since we know `alen` can't exceed the size of the buffer,
			// it is fine to use `unwrap()` here.
			buf.get_mut(..len).unwrap().copy_from_slice(src);

			bear_ssl_sys::br_ssl_engine_recvapp_ack(
				&mut self.ssl_context.eng as *mut _,
				len as bear_ssl_sys::size_t,
			);
			Ok(len)
		}
	}
}

impl<'a> core2::io::Write for ClientSession<'a> {
	/// If the client is not yet ready to write data, an error with kind [`core2::io::Error::WouldBlock`] will be returned.
	///
	/// [`core2::io::Error::WouldBlock`]: ../../core2/enum.ErrorKind.html#variant.WouldBlock
	fn write(&mut self, buf: &[u8]) -> core2::io::Result<usize> {
		if self.br_state() & bear_ssl_sys::BR_SSL_SENDAPP == 0 {
			return Err(core2::io::Error::from(core2::io::ErrorKind::WouldBlock));
		} else if buf.is_empty() {
			return Ok(0);
		}

		unsafe {
			let mut len = 0 as bear_ssl_sys::size_t;
			let dst = NonNull::new(bear_ssl_sys::br_ssl_engine_sendapp_buf(
				&mut self.ssl_context.eng as *mut _,
				&mut len as *mut _,
			))
			.unwrap();

			let len = core::cmp::min(len as usize, buf.len());
			let dst = core::slice::from_raw_parts_mut(dst.as_ptr(), len);
			// Since we know `alen` can't exceed the size of the buffer,
			// it is fine to use `unwrap()` here.
			dst.copy_from_slice(buf.get(..len).unwrap());

			bear_ssl_sys::br_ssl_engine_sendapp_ack(
				&mut self.ssl_context.eng as *mut _,
				len as bear_ssl_sys::size_t,
			);
			Ok(len)
		}
	}

	fn flush(&mut self) -> core2::io::Result<()> {
		unsafe {
			bear_ssl_sys::br_ssl_engine_flush(&mut self.ssl_context.eng as *mut _, 0);
		}
		Ok(())
	}
}

impl<'a> core::fmt::Display for ClientSession<'a> {
	fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
		let server_name = {
			let len = self
				.ssl_context
				.eng
				.server_name
				.iter()
				.take_while(|&&x| x != 0)
				.count();

			if len > 0 {
				unsafe {
					core::str::from_utf8_unchecked(core::slice::from_raw_parts(
						self.ssl_context.eng.server_name.as_ptr() as *const u8,
						len,
					))
				}
			} else {
				""
			}
		};

		write!(
			f,
			"ClientSession -> \"{}\" [{}] ",
			server_name,
			if self.is_active {
				if self.handshake_completed {
					"ACTIVE"
				} else {
					"ACTIVE (Handshake in progress)"
				}
			} else {
				"INACTIVE"
			},
		)
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	#[test]
	#[should_panic(expected = "Could not create ClientStream: MissingRequired")]
	#[cfg(feature = "std")]
	fn construct_context_without_required_field() {
		let mut io_buf = [0u8; 16];
		ClientConfig::new()
			.start(&mut io_buf[..])
			.expect("Could not create ClientStream");
	}

	#[test]
	#[should_panic(expected = "Could not create ClientStream: InvalidParam")]
	#[cfg(feature = "std")]
	fn construct_context_with_invalid_buffer() {
		let mut io_buf = [0u8; 16];
		ClientConfig::new()
			.current_time(std::time::SystemTime::now())
			.entropy_source(EntropySource::OsImpl)
			.trust_anchors(&[])
			.start(&mut io_buf[..])
			.expect("Could not create ClientStream");
	}
}

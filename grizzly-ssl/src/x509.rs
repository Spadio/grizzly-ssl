//! X.509 public key certificate format.
//!
//! This module handles the types and function necessary to
//! define and implement public keys.
//!
//! Public keys are necessary for a client to know, in order
//! to perform the assymetric cryptography involved in the handshake;
//! the server shows its certificate to the client, and that certificate
//! contains the server's public key. The client will trust that information
//! (i.e. accept to use that public key as the genuine server's public key),
//! after a process called [validation][1].
//!
//! SSL/TLS specifies that each server should provide its certificate as
//! part of an already built and valid chain. A client is then allowed to
//! validate just that chain, in due order.
//!
//! [1]: https://tools.ietf.org/html/rfc5280#section-6

use crate::error::Error;
use bear_ssl_sys::{
	br_ec_public_key, br_rsa_private_key, br_rsa_public_key, br_ssl_client_context, br_x500_name,
	br_x509_certificate, br_x509_pkey, br_x509_pkey__bindgen_ty_1, br_x509_trust_anchor,
};
use core::marker::PhantomData;

/// A public key for assymetric cryptography.
#[derive(Copy, Clone, Debug, Eq, PartialEq)]
pub enum PublicKey<'a> {
	/// Elliptic-curve encryption.
	Ec {
		/// Identifier (NID) for the curve used by this key.
		curve: i32,
		/// Public curve point.
		q: &'a [u8],
	},
	/// RSA encryption.
	Rsa {
		/// Modulus.
		n: &'a [u8],
		/// Exponent.
		e: &'a [u8],
	},
}

impl<'a> PublicKey<'a> {
	pub(crate) const fn into_br_pkey(self) -> br_x509_pkey {
		match self {
			PublicKey::Ec { curve, q } => {
				// Unfortunately, the C headers want a mut pointer, but the C source code does not require it,
				// so this is safe(ish).
				let q_ptr = q.as_ptr() as *mut u8;

				br_x509_pkey {
					key_type: bear_ssl_sys::BR_KEYTYPE_EC as u8,
					key: br_x509_pkey__bindgen_ty_1 {
						ec: br_ec_public_key {
							curve,
							q: q_ptr,
							qlen: q.len() as bear_ssl_sys::size_t,
						},
					},
				}
			}
			PublicKey::Rsa { n, e } => {
				let n_ptr = n.as_ptr() as *mut u8;
				let e_ptr = e.as_ptr() as *mut u8;

				br_x509_pkey {
					key_type: bear_ssl_sys::BR_KEYTYPE_RSA as u8,
					key: br_x509_pkey__bindgen_ty_1 {
						rsa: br_rsa_public_key {
							n: n_ptr,
							nlen: n.len() as bear_ssl_sys::size_t,
							e: e_ptr,
							elen: e.len() as bear_ssl_sys::size_t,
						},
					},
				}
			}
		}
	}
}

/// A private key for assymetric cryptography.
#[derive(Clone, Debug)]
#[repr(C)]
pub enum PrivateKey<'a> {
	/// Elliptic-curve encryption.
	Ec {
		/// Identifier (NID) for the curve used by this key.
		curve: i32,
		/// Private key.
		x: &'a [u8],
	},
	/// RSA encryption.
	Rsa {
		/// Modulus bit length (must be exact).
		n_bitlen: u32,
		/// First prime factor.
		p: &'a [u8],
		/// Second prime factor
		q: &'a [u8],
		/// First reduced private exponent.
		dp: &'a [u8],
		/// Second reduced private exponent.
		dq: &'a [u8],
		/// CRT coefficient.
		iq: &'a [u8],
	},
}

/// A X.509 certificate object.
#[repr(C)]
#[derive(Clone, Debug)]
pub struct X509Certificate<'a> {
	raw: br_x509_certificate,
	_phantom: PhantomData<&'a [u8]>,
}

impl<'a> X509Certificate<'a> {
	/// Construct a new X.509 certificate object from raw DER data.
	pub const fn new(data: &'a [u8]) -> Self {
		let data_ptr = data.as_ptr() as *mut u8;
		Self {
			raw: br_x509_certificate {
				data: data_ptr,
				data_len: data.len() as bear_ssl_sys::size_t,
			},
			_phantom: PhantomData,
		}
	}
}

unsafe impl<'a> Send for X509Certificate<'a> {}
unsafe impl<'a> Sync for X509Certificate<'a> {}

/// A TLS client certificate.
///
/// In basic TLS usage, the client validates the server's certificate chain, extracts the public key from the
/// provided certificate, and then uses it in some way in the handshake. This means that, while the client
/// authenticates the server, and gains some guarantee that any data sent through the TLS connection will
/// only be readable by the intended server. However, from the server's point of view, the client is anonymous.
///
/// Client certificates allow client authentication to be performed during the TLS handshake, instead of within
/// the TLS tunnel itself. A client certificate consists of a certificate chain, and a corresponding private key.
/// The client uses the private key to convince the server that it really controls the private key that matches
/// the public key in the end-entity (EE) certificate.
///
/// # Build Scripts
///
/// It is often desirable to generate the structures for certificates and keys at compile-time. To this end,
/// the [`grizzly-ssl-parsers`] crate can be used, and the exposed methods can be called from a build script.
/// For more information, see the crate documentation
///
/// [`grizzly-ssl-parsers`]: https://docs.rs/grizzly-ssl-parsers
#[derive(Clone, Debug)]
pub struct ClientCertificate<'a> {
	chain: &'a [X509Certificate<'a>],
	priv_key: PrivateKey<'a>,
}

impl<'a> ClientCertificate<'a> {
	/// Construct a client certificate from a chain of X.509 certificates, and a private key.
	pub const fn new(chain: &'a [X509Certificate<'a>], priv_key: PrivateKey<'a>) -> Self {
		Self { chain, priv_key }
	}

	pub(crate) unsafe fn set_br_ssl_context(
		&self,
		ssl_context: &mut br_ssl_client_context,
	) -> Result<(), Error> {
		// Get a reference to `n_bitlen`, so that we can get a pointer to it.
		// We do this so we have a pointer to the fields, as the Rust enum
		// puts some things in memory before the actual fields.
		match &self.priv_key {
			PrivateKey::Ec { .. } => Err(Error::UnsupportedOperation),
			PrivateKey::Rsa { n_bitlen, .. } => {
				bear_ssl_sys::br_ssl_client_set_single_rsa(
					ssl_context as *mut _,
					self.chain.as_ptr() as *const _,
					self.chain.len() as bear_ssl_sys::size_t,
					n_bitlen as *const _ as *const br_rsa_private_key,
					bear_ssl_sys::br_rsa_pkcs1_sign_get_default(),
				);
				Ok(())
			}
		}
	}
}

/// A known and trusted public key.
///
/// Server public keys are obtained through certificates, which must be validated,
/// and that process is recursive. It stops on a trust anchor, which is a public key
/// that the validator already knows and trusts.
///
/// Trust anchors are often called "root certificates" and typically come preinstalled
/// in operating systems and web browsers; similarly, a non-root certificate is
/// often called an "intermediate CA". Validation is thus applied on a *certificate chain*
/// that goes from the trust anchor, down to the certificate we really are interested in,
/// and called the "end-entity" (EE), because it is at the end of the chain.
///
/// This implementation of a trust anchor does not require a full X.509 root certificate;
/// it only requries the bare mininum for perform the necessary cryptographic functions
/// for TLS. This means the "Subject DN", and the raw mathmatical constants involved in
/// the cryptographic process of whichever cryptosystem the public key uses.
///
/// # Build Scripts
///
/// It is often desirable to generate the structures for certificates and keys at compile-time. To this end,
/// the [`grizzly-ssl-parsers`] crate can be used, and the exposed methods can be called from a build script.
/// For more information, see the crate documentation
///
/// # Examples
///
/// Creating a trust anchor for a root CA that uses RSA:
/// ```rust
/// # use grizzly_ssl::x509::{PublicKey, TrustAnchor};
/// static ROOT_DN: [u8; 13] = *b"NotAnActualDN";
/// static ROOT_N: [u8; 256] = [0; 256];
/// static ROOT_E: [u8; 4] = [0x00, 0x01, 0x00, 0x01];
///
/// static TRUST_ANCHORS: [TrustAnchor; 1] = [TrustAnchor::new(&ROOT_DN, true, PublicKey::Rsa {
///     n: &ROOT_N,
///     e: &ROOT_E,
/// })];
/// ```
///
/// [`grizzly-ssl-parsers`]: https://docs.rs/grizzly-ssl-parsers
#[repr(C)]
#[derive(Clone)]
pub struct TrustAnchor<'a> {
	raw: br_x509_trust_anchor,
	_phantom: PhantomData<&'a [u8]>,
}

impl<'a> TrustAnchor<'a> {
	/// Constructs a new `TrustAnchor` from a "Subject DN", and a [`PublicKey`].
	///
	/// The `ca` flag indicates whether or not the trust anchor is a "Certificate Authority".
	/// A "CA" trust anchor is deemed fit to verify signatures on certificates, whereas
	/// a "non-CA" trust anchor can only be used for direct trust (server's certificate
	/// name and key match the anchor).
	///
	/// [`PublicKey`]: enum.PublicKey.html
	pub const fn new(dn: &'a [u8], ca: bool, public_key: PublicKey<'a>) -> Self {
		let flags = {
			if ca {
				bear_ssl_sys::BR_X509_TA_CA
			} else {
				0
			}
		};
		let dn_ptr = dn.as_ptr() as *mut u8;

		Self {
			raw: br_x509_trust_anchor {
				dn: br_x500_name {
					data: dn_ptr,
					len: dn.len() as bear_ssl_sys::size_t,
				},
				flags,
				pkey: public_key.into_br_pkey(),
			},
			_phantom: PhantomData,
		}
	}
}

unsafe impl<'a> Send for TrustAnchor<'a> {}
unsafe impl<'a> Sync for TrustAnchor<'a> {}

impl<'a> core::fmt::Debug for TrustAnchor<'a> {
	fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
		match self.raw.pkey.key_type as u32 {
			bear_ssl_sys::BR_KEYTYPE_EC => f
				.debug_struct("TrustAnchor")
				.field("dn", &self.raw.dn)
				.field("pkey", unsafe { &self.raw.pkey.key.ec })
				.finish(),
			bear_ssl_sys::BR_KEYTYPE_RSA => f
				.debug_struct("TrustAnchor")
				.field("dn", &self.raw.dn)
				.field("pkey", unsafe { &self.raw.pkey.key.rsa })
				.finish(),
			_ => unreachable!(),
		}
	}
}

#[cfg(test)]
mod tests {
	use super::*;

	static DN: &[u8] = &[0xDE, 0xAD, 0xBE, 0xEF];
	static PUB_KEY_EC: PublicKey = PublicKey::Ec {
		curve: 0,
		q: &[0; 256],
	};
	static PUB_KEY_RSA: PublicKey = PublicKey::Rsa {
		n: &[0; 256],
		e: &[0, 1, 0, 1],
	};
	#[test]
	fn ec_pub_key_to_br_pub_key() {
		if let PublicKey::Ec { curve, q } = PUB_KEY_EC {
			let br_pub_key = PUB_KEY_EC.into_br_pkey();
			assert_eq!(br_pub_key.key_type, bear_ssl_sys::BR_KEYTYPE_EC as u8);

			let br_pub_key_inner = unsafe { br_pub_key.key.ec };
			assert_eq!(br_pub_key_inner.curve, curve);
			assert_eq!(br_pub_key_inner.q, q.as_ptr() as *mut u8);
			assert_eq!(br_pub_key_inner.qlen, q.len() as bear_ssl_sys::size_t);
		} else {
			unreachable!();
		}
	}

	#[test]
	fn rsa_pub_key_to_br_pub_key() {
		if let PublicKey::Rsa { n, e } = PUB_KEY_RSA {
			let br_pub_key = PUB_KEY_RSA.into_br_pkey();
			assert_eq!(br_pub_key.key_type, bear_ssl_sys::BR_KEYTYPE_RSA as u8);

			let br_pub_key_inner = unsafe { br_pub_key.key.rsa };
			assert_eq!(br_pub_key_inner.n, n.as_ptr() as *mut u8);
			assert_eq!(br_pub_key_inner.nlen, n.len() as bear_ssl_sys::size_t);
			assert_eq!(br_pub_key_inner.e, e.as_ptr() as *mut u8);
			assert_eq!(br_pub_key_inner.elen, e.len() as bear_ssl_sys::size_t);
		} else {
			unreachable!();
		}
	}

	#[test]
	fn construct_ca_trust_anchor() {
		let trust_anchor = TrustAnchor::new(&DN, true, PUB_KEY_RSA);
		assert_eq!(trust_anchor.raw.dn.data, DN.as_ptr() as *mut u8);
		assert_eq!(trust_anchor.raw.dn.len, DN.len() as bear_ssl_sys::size_t);
		assert_eq!(trust_anchor.raw.flags, bear_ssl_sys::BR_X509_TA_CA);

		if let PublicKey::Rsa { n, e } = PUB_KEY_RSA {
			let ta_pkey = trust_anchor.raw.pkey;
			let ta_pkey_inner = unsafe { ta_pkey.key.rsa };

			assert_eq!(ta_pkey.key_type, bear_ssl_sys::BR_KEYTYPE_RSA as u8);
			assert_eq!(ta_pkey_inner.n, n.as_ptr() as *mut u8);
			assert_eq!(ta_pkey_inner.nlen, n.len() as bear_ssl_sys::size_t);
			assert_eq!(ta_pkey_inner.e, e.as_ptr() as *mut u8);
			assert_eq!(ta_pkey_inner.elen, e.len() as bear_ssl_sys::size_t);
		} else {
			unreachable!();
		}
	}
}

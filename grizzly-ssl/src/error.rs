use bear_ssl_sys::br_ssl_engine_context;

/// The error type for the library.
#[derive(Debug)]
pub enum Error {
	/// Attempted to perform an operation on a session that has already been closed.
	Closed,
	/// The internal engine reported an error (typically, this means there was a TLS error).
	EngineFailure(BearError),
	/// A provided parameter was invalid.
	InvalidParam,
	/// An error occurred while trying to perform an I/O operation.
	Io(bare_io::Error),
	/// A requried field was missing.
	MissingRequired,
	/// The session is not ready to perform the requested operation.
	NotReady,
	/// Attempted to perform an action that the library doesn't support.
	UnsupportedOperation,
}

impl Error {
	pub(crate) fn from_engine(ssl_context: &br_ssl_engine_context) -> Self {
		// The bitwise AND gets rid of the 'fatal error' flags,
		// so that we can match the error code to an enum variant easily.
		match ssl_context.err & 0xFF {
			0 | 32 => Error::Closed,
			1 => Error::EngineFailure(BearError::SslBadParam),
			2 => Error::EngineFailure(BearError::SslBadState),
			3 => Error::EngineFailure(BearError::SslUnsupportedVersion),
			4 => Error::EngineFailure(BearError::SslBadVersion),
			5 => Error::EngineFailure(BearError::SslBadLength),
			6 => Error::EngineFailure(BearError::SslTooLarge),
			7 => Error::EngineFailure(BearError::SslBadMac),
			8 => Error::EngineFailure(BearError::SslNoRandom),
			9 => Error::EngineFailure(BearError::SslUnknownType),
			10 => Error::EngineFailure(BearError::SslUnexpected),
			12 => Error::EngineFailure(BearError::SslBadCcs),
			13 => Error::EngineFailure(BearError::SslBadAlert),
			14 => Error::EngineFailure(BearError::SslBadHandshake),
			15 => Error::EngineFailure(BearError::SslOversizedId),
			16 => Error::EngineFailure(BearError::SslBadCipherSuite),
			17 => Error::EngineFailure(BearError::SslBadCompression),
			18 => Error::EngineFailure(BearError::SslBadFragLen),
			19 => Error::EngineFailure(BearError::SslBadSecReneg),
			20 => Error::EngineFailure(BearError::SslExtraExtension),
			21 => Error::EngineFailure(BearError::SslBadSni),
			22 => Error::EngineFailure(BearError::SslBadHelloDone),
			23 => Error::EngineFailure(BearError::SslLimitExceeded),
			24 => Error::EngineFailure(BearError::SslBadFinished),
			25 => Error::EngineFailure(BearError::SslResumeMismatch),
			26 => Error::EngineFailure(BearError::SslInvalidAlgorithm),
			27 => Error::EngineFailure(BearError::SslBadSignature),
			33 => Error::EngineFailure(BearError::X509InvalidValue),
			34 => Error::EngineFailure(BearError::X509Truncated),
			35 => Error::EngineFailure(BearError::X509EmptyChain),
			36 => Error::EngineFailure(BearError::X509InnerTrunc),
			37 => Error::EngineFailure(BearError::X509BadTagClass),
			38 => Error::EngineFailure(BearError::X509BadTagValue),
			39 => Error::EngineFailure(BearError::X509IndefiniteLength),
			40 => Error::EngineFailure(BearError::X509ExtraElement),
			41 => Error::EngineFailure(BearError::X509Unexpected),
			42 => Error::EngineFailure(BearError::X509NotConstructed),
			43 => Error::EngineFailure(BearError::X509NotPrimitive),
			44 => Error::EngineFailure(BearError::X509PartialByte),
			45 => Error::EngineFailure(BearError::X509BadBoolean),
			46 => Error::EngineFailure(BearError::X509Overflow),
			47 => Error::EngineFailure(BearError::X509BadDn),
			48 => Error::EngineFailure(BearError::X509BadTime),
			49 => Error::EngineFailure(BearError::X509Unsupported),
			50 => Error::EngineFailure(BearError::X509LimitExceeded),
			51 => Error::EngineFailure(BearError::X509WrongKeyType),
			52 => Error::EngineFailure(BearError::X509BadSignature),
			53 => Error::EngineFailure(BearError::X509TimeUnknown),
			54 => Error::EngineFailure(BearError::X509Expired),
			55 => Error::EngineFailure(BearError::X509DnMismatch),
			56 => Error::EngineFailure(BearError::X509BadServerName),
			57 => Error::EngineFailure(BearError::X509CriticalExtension),
			58 => Error::EngineFailure(BearError::X509NotCa),
			59 => Error::EngineFailure(BearError::X509ForbiddenKeyUsage),
			60 => Error::EngineFailure(BearError::X509WeakPublicKey),
			62 => Error::EngineFailure(BearError::X509NotTrusted),
			_ => Error::EngineFailure(BearError::Unknown(ssl_context.err)),
		}
	}
}

impl core::fmt::Display for Error {
	fn fmt(&self, f: &mut core::fmt::Formatter<'_>) -> core::fmt::Result {
		write!(f, "{:?}", self)
	}
}

#[cfg(feature = "std")]
impl std::error::Error for Error {}

/// Represents all of the possible errors that the BearSSL library can return.
#[derive(Debug)]
pub enum BearError {
	/// Caller-provided parameter is incorrect.
	SslBadParam,
	/// Operation requested by the called cannot be applied with the current
	/// context state (e.g. reading data while outgoing data is waiting to
	/// be sent).
	SslBadState,
	/// Incoming protocol or record version is unsupported.
	SslUnsupportedVersion,
	/// Incoming record does not match the expected version.
	SslBadVersion,
	/// Incoming record length is invalid.
	SslBadLength,
	/// Incoming record is too large to be processed, or buffer is too small
	/// for the handshake message to send.
	SslTooLarge,
	/// Decryption found an invalid padding, or the record MAC is not correct.
	SslBadMac,
	/// No initial entropy was provided, and none can be obtained from the OS.
	SslNoRandom,
	/// Incoming record type is unknown.
	SslUnknownType,
	/// Incoming record or message has wrong type with regards to the current engine state.
	SslUnexpected,
	/// ChangeCipherSpec message from the peer has invalid contents.
	SslBadCcs,
	/// Alert message from the peer has invalid contents (odd length).
	SslBadAlert,
	/// Incoming handshake message decoding failed.
	SslBadHandshake,
	/// ServerHello contains a session ID which is larger than 32 bytes.
	SslOversizedId,
	/// Server wants to use a cipher suite that we did not claim to support.
	/// this is also reported if we tried to advertise a cipher suite that
	/// we do not support.
	SslBadCipherSuite,
	/// Server wants to use a compression that we did not claim to support.
	SslBadCompression,
	/// Server's max fragment length does not match client's.
	SslBadFragLen,
	/// Secure renegotiation failed.
	SslBadSecReneg,
	/// Server sent an extension type that we did not announce, or used the
	/// same extension type several times in a single ServerHello.
	SslExtraExtension,
	/// Invalid Server Name Indication contents (when used by the server,
	/// this extension shall be empty).
	SslBadSni,
	/// Invalid ServerHelloDone from the server (length is not 0).
	SslBadHelloDone,
	/// Internal limit exceeded (e.g. server's public key is too large).
	SslLimitExceeded,
	/// Finished message from peer does not match the expected value.
	SslBadFinished,
	/// Session resumption attempt with distinct version or cipher suite.
	SslResumeMismatch,
	/// Unsupported or invalid algorithm (ECDHE curve, signature algorithm,
	/// hash function).
	SslInvalidAlgorithm,
	/// Invalid signature on ServerKeyExchange message.
	SslBadSignature,

	/// X.509 status: invalid value in an ASN.1 structure.
	X509InvalidValue,
	/// X.509 status: truncated certificate.
	X509Truncated,
	/// X.509 status: empty certificate chain (no certificate at all).
	X509EmptyChain,
	/// X.509 status: decoding error: inner element extends beyond outer element size.
	X509InnerTrunc,
	/// X.509 status: decoding error: unsupported tag class (application or private).
	X509BadTagClass,
	/// X.509 status: decoding error: unsupported tag value.
	X509BadTagValue,
	/// X.509 status: decoding error: indefinite length.
	X509IndefiniteLength,
	/// X.509 status: decoding error: extraneous element.
	X509ExtraElement,
	/// X.509 status: decoding error: unexpected element.
	X509Unexpected,
	/// X.509 status: decoding error: expected constructed element, but is primitive.
	X509NotConstructed,
	/// X.509 status: decoding error: expected primitive element, but is constructed.
	X509NotPrimitive,
	/// X.509 status: decoding error: BIT STRING length is not multiple of 8.
	X509PartialByte,
	/// X.509 status: decoding error: BOOLEAN value has invalid length.
	X509BadBoolean,
	/// X.509 status: decoding error: value is off-limits.
	X509Overflow,
	/// X.509 status: invalid distinguished name.
	X509BadDn,
	/// X.509 status: invalid date/time representation.
	X509BadTime,
	/// X.509 status: certificate contains unsupported features that cannot be ignored.
	X509Unsupported,
	/// X.509 status: key or signature size exceeds internal limits.
	X509LimitExceeded,
	/// X.509 status: key type does not match that which was expected.
	X509WrongKeyType,
	/// X.509 status: signature is invalid.
	X509BadSignature,
	/// X.509 status: validation time is unknown.
	X509TimeUnknown,
	/// X.509 status: certificate is expired or not yet valid.
	X509Expired,
	/// X.509 status: issuer/subject DN mismatch in the chain.
	X509DnMismatch,
	/// X.509 status: expected server name was not found in the chain.
	X509BadServerName,
	/// X.509 status: unknown critical extension in certificate.
	X509CriticalExtension,
	/// X.509 status: not a CA, or path length constraint violation.
	X509NotCa,
	/// X.509 status: Key Usage extension prohibits intended usage.
	X509ForbiddenKeyUsage,
	/// X.509 status: public key found in certificate is too small.
	X509WeakPublicKey,
	/// X.509 status: chain could not be linked to a trust anchor.
	X509NotTrusted,

	#[doc(hidden)]
	Unknown(i32),
}
